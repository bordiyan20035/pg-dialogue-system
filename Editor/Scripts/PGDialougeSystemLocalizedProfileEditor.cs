using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace PG.DialougeSystem
{
    [CustomEditor(typeof(PGDialougeSystemLocalizedProfile))]
    public class PGDialougeSystemLocalizedProfileEditor : Editor
    {

        private PGDialougeSystemProfile _profile;

        private void OnEnable()
        {
            _profile = (PGDialougeSystemProfile)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Open Window"))
            {
                PGDialougeSystemEditorWindow.OpenWindow(_profile);
            }
        }
        [OnOpenAsset]
        //Handles opening the editor window when double-clicking project files
        public static bool OnOpenAsset(int instanceID, int line)
        {
            Object obj = EditorUtility.InstanceIDToObject(instanceID);
            if (obj is PGDialougeSystemProfile)
            {
                PGDialougeSystemProfile asset = (PGDialougeSystemProfile)obj;
                PGDialougeSystemEditorWindow.OpenWindow(asset);
                return true;
            }
            return false;
        }
    }
}