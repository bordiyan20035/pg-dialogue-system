using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using Unity.VisualScripting.YamlDotNet.Core.Tokens;
using UnityEngine.Timeline;
using UnityEngine.Profiling;
using UnityEditor.PackageManager.UI;
using static Codice.Client.Common.DiffMergeToolConfig;
using UnityEngine.TextCore.Text;

namespace PG.DialougeSystem
{
    public class PGDialougeSystemEditorWindow : EditorWindow
    {
        private static PGDialougeSystemProfile _profile;
        private int _currentDialouge;

        private GUIStyle _headerStyle;
        private GUIStyle _subheaderStyle;

        private Vector2 _scrollDialouges;
        private Vector2 _scrollDialouge;

        private GUIStyle _panelStyle;
        private void OnEnable()
        {
            _headerStyle = new GUIStyle()
            {
                fontSize = 24,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleCenter
            };
            _headerStyle.normal.textColor = Color.white;
            _subheaderStyle = new GUIStyle()
            {
                fontSize = 18,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleCenter
            };
            _subheaderStyle.normal.textColor = Color.white;

            // ������� ����� ��� ������ � ��������
            _panelStyle = new GUIStyle(GUI.skin.box);
            _panelStyle.margin = new RectOffset(10, 10, 10, 10);
            _panelStyle.padding = new RectOffset(10, 10, 10, 10);
            _panelStyle.border = new RectOffset(2, 2, 2, 2);
            _panelStyle.normal.background = Texture2D.grayTexture; // ����� ���� ����
        }
        public static void OpenWindow(PGDialougeSystemProfile profile)
        {
            var window = GetWindow<PGDialougeSystemEditorWindow>(profile.name);
            window.titleContent.image = (Texture)Resources.Load("PG/Dialouge System/Icon");
            _profile = profile;
        }
        [MenuItem("Window/PG/Dialouge System/Open Current Window")]
        public static void OpenWindow()
        {
            var window = GetWindow<PGDialougeSystemEditorWindow>();
            if (_profile != null)
            {
                window.titleContent.text = _profile.name;
            }
            window.titleContent.image = (Texture)Resources.Load("PG/Dialouge System/Icon");
        }
        [MenuItem("Window/PG/Dialouge System/Open Empty Window")]
        public static void OpenEmptyWindow()
        {
            var window = GetWindow<PGDialougeSystemEditorWindow>();
            window.titleContent.image = (Texture)Resources.Load("PG/Dialouge System/Icon");
            _profile = null;
        }
        private void OnGUI()
        {
            if (_profile == null)
            {
                OnEmptyPanel();
            }
            else
            {
                OnNotEmptyPanel();
            }
        }
        void OnEmptyPanel()
        {
            GUILayout.Label("Dialouge System", _headerStyle);
            GUILayout.Space(20);
            _profile = (PGDialougeSystemProfile)EditorGUILayout.ObjectField("Profile", _profile, typeof(PGDialougeSystemProfile), false);
        }
        void OnNotEmptyPanel()
        {
            GUILayout.BeginHorizontal();

            GUILayout.BeginVertical("box", GUILayout.MinWidth(200), GUILayout.MaxWidth(325));
            GUILayout.Label("Dialouges", _headerStyle);
            ActionsDialouges();
            _scrollDialouges = GUILayout.BeginScrollView(_scrollDialouges);
            DisplayDialouges();
            GUILayout.EndScrollView();
            GUILayout.EndVertical();

            GUILayout.BeginVertical("box");
            GUILayout.Label("Dialouge", _headerStyle);
            if (_profile.dialouges.Count > 0)
            {
                _scrollDialouge = GUILayout.BeginScrollView(_scrollDialouge);
                DisplayDialouge();
                GUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            Undo.RecordObject(_profile, "Change Dialouge System Profile");
        }
        void ActionsDialouges()
        {
            GUILayout.BeginHorizontal("box");
            if (GUILayout.Button("+"))
            {
                AddDialouge();
                GUI.FocusControl(null);
            }
            if (GUILayout.Button("-"))
            {
                RemoveDialouge(_currentDialouge);
                GUI.FocusControl(null);
            }
            GUILayout.EndHorizontal();
        }
        void DisplayDialouge()
        {
            GUIStyle style = new GUIStyle(EditorStyles.textArea);
            style.wordWrap = true;
            _profile.dialouges[_currentDialouge].name = EditorGUILayout.TextField("Name", _profile.dialouges[_currentDialouge].name);
            GUILayout.Space(10);
            GUILayout.Label("Description");
            _profile.dialouges[_currentDialouge].description = EditorGUILayout.TextArea(_profile.dialouges[_currentDialouge].description, style, GUILayout.MinHeight(40));

            _profile.dialouges[_currentDialouge].eventName = EditorGUILayout.TextField("Event Name", _profile.dialouges[_currentDialouge].eventName);

            GUILayout.Space(20);
            _profile.dialouges[_currentDialouge].cutscene = (TimelineAsset)EditorGUILayout.ObjectField("Cutscene", _profile.dialouges[_currentDialouge].cutscene, typeof(TimelineAsset), false);

            GUILayout.Space(20);
            _profile.dialouges[_currentDialouge].clip = (AudioClip)EditorGUILayout.ObjectField("Audio clip", _profile.dialouges[_currentDialouge].clip, typeof(AudioClip), false);


            GUILayout.Space(40);
            GUILayout.Label("Camera movement", _subheaderStyle);
            _profile.dialouges[_currentDialouge].virtualCameraName = EditorGUILayout.TextField("VirtualCamera Name", _profile.dialouges[_currentDialouge].virtualCameraName);
            GUILayout.Space(40);

            DisplayCharacters();
        }
        void DisplayCharacters()
        {
            GUILayout.Label("Characters", _subheaderStyle);
            GUILayout.BeginVertical("box");
            if (GUILayout.Button("Add Character"))
            {
                _profile.dialouges[_currentDialouge].characters.Add(new Character());
                EditorUtility.SetDirty(_profile);
                GUI.FocusControl(null);
            }
            GUILayout.Space(40);
            if (_profile.dialouges[_currentDialouge].characters.Count > 0)
            {
                for (int i = 0; i < _profile.dialouges[_currentDialouge].characters.Count; i++)
                {
                    Character character = _profile.dialouges[_currentDialouge].characters[i];

                    GUILayout.BeginVertical(_panelStyle);
                    if (!string.IsNullOrWhiteSpace(character.name))
                    {
                        GUILayout.Label(character.name, _subheaderStyle);
                    }
                    else
                    {
                        GUILayout.Label("Character " + i, _subheaderStyle);
                    }
                    character.name = EditorGUILayout.TextField("Name", character.name);

                    if (GUILayout.Button("Add parameter"))
                    {
                        character.animationInvokers.Add(new Character.AnimationInvoker());
                        EditorUtility.SetDirty(_profile);
                        GUI.FocusControl(null);
                    }

                    GUILayout.Space(20);

                    for (int a = 0; a < character.animationInvokers.Count; a++)
                    {
                        Character.AnimationInvoker animationInvoker = character.animationInvokers[a];

                        GUILayout.BeginHorizontal(_panelStyle);
                        GUILayout.BeginVertical("box");
                        animationInvoker.animationParameter = EditorGUILayout.TextField("Parameter", animationInvoker.animationParameter);


                        animationInvoker.type = (Character.AnimationType)EditorGUILayout.EnumPopup("Type", animationInvoker.type);

                        switch (animationInvoker.type)
                        {
                            case Character.AnimationType.Int:
                                animationInvoker.animationInt = EditorGUILayout.IntField("Set Value", animationInvoker.animationInt);
                                break;
                            case Character.AnimationType.Float:
                                animationInvoker.animationFloat = EditorGUILayout.FloatField("Set Value", animationInvoker.animationFloat);
                                break;
                            case Character.AnimationType.Bool:
                                animationInvoker.animationBool = EditorGUILayout.Toggle("Set Value", animationInvoker.animationBool);
                                break;
                        }
                        GUILayout.EndVertical();
                        if (GUILayout.Button("X", GUILayout.Width(30), GUILayout.Height(30)))
                        {
                            character.animationInvokers.RemoveAt(i);
                            EditorUtility.SetDirty(_profile);
                            GUI.FocusControl(null);
                        }
                        GUILayout.EndHorizontal();
                        GUILayout.Space(10);
                    }
                    if (GUILayout.Button("X"))
                    {
                        _profile.dialouges[_currentDialouge].characters.RemoveAt(i);
                        EditorUtility.SetDirty(_profile);
                        GUI.FocusControl(null);
                    }

                    GUILayout.EndVertical();
                    GUILayout.Space(25);
                }
            }

            EditorUtility.SetDirty(_profile);
            GUILayout.EndVertical();
        }
        void DisplayDialouges()
        {
            //GUILayout.Label(_currentDialouge.ToString());
            if (_profile.dialouges.Count > 0)
            {
                for (int i = 0; i < _profile.dialouges.Count; i++)
                {
                    GUILayout.BeginVertical("box");

                    if (i == _currentDialouge)
                    {
                        GUI.backgroundColor = new Color(0.5f, 0.75f, 1f);
                    }
                    else
                    {
                        GUI.backgroundColor = Color.white;
                    }
                    GUILayout.BeginHorizontal("box");
                    string description;
                    if (string.IsNullOrWhiteSpace(_profile.dialouges[i].description))
                    {
                        description = $"State {i}";
                    }
                    else
                    {
                        description = _profile.dialouges[i].description;
                        int MaxLength = 20;
                        if (description.Length > MaxLength)
                        {
                            description = description.Substring(0, MaxLength);
                            description += "...";
                        }
                    }
                    if (GUILayout.Button(description, GUILayout.MaxWidth(200)))
                    {
                        _currentDialouge = i;
                        GUI.FocusControl(null);
                    }
                    if (i < _profile.dialouges.Count - 1)
                    {
                        if (GUILayout.Button((Texture)Resources.Load("PG/Dialouge System/Arrow Down"), GUILayout.MaxHeight(20), GUILayout.MaxWidth(30)))
                        {
                            SortDown(i);
                            GUI.FocusControl(null);
                        }
                    }
                    if (i > 0)
                    {
                        if (GUILayout.Button((Texture)Resources.Load("PG/Dialouge System/Arrow Up"), GUILayout.MaxHeight(20), GUILayout.MaxWidth(30)))
                        {
                            SortUp(i);
                            GUI.FocusControl(null);
                        }
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                }
                GUI.backgroundColor = Color.white;
            }
        }
        void SortUp(int value)
        {
            Dialouge dialouge = _profile.dialouges[value];
            _profile.dialouges[value] = _profile.dialouges[value-1];
            _profile.dialouges[value-1] = dialouge;
            EditorUtility.SetDirty(_profile);
        }
        void SortDown(int value)
        {
            Dialouge dialouge = _profile.dialouges[value];
            _profile.dialouges[value] = _profile.dialouges[value+1];
            _profile.dialouges[value+1] = dialouge;
            EditorUtility.SetDirty(_profile);
        }
        void AddDialouge()
        {
            if (_profile.dialouges.Count == 0)
            {
                _currentDialouge = 0;
                _profile.dialouges.Add(new Dialouge());
            }
            else
            {
                _profile.dialouges.Insert(_currentDialouge + 1 ,new Dialouge(_profile.dialouges[_currentDialouge]));
            }
            EditorUtility.SetDirty(_profile);
        }
        void RemoveDialouge(int value)
        {
            if (_currentDialouge > 0)
            {
                _currentDialouge--;
            }
            if (_currentDialouge == 0)
            {
                _currentDialouge = 0;
            }
            if (_profile.dialouges.Count > 0)
            {
                _profile.dialouges.RemoveAt(value);
            }
            EditorUtility.SetDirty(_profile);
        }
    }
}