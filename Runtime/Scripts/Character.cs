﻿using System.Collections.Generic;
using UnityEngine;

namespace PG.DialougeSystem
{
    [System.Serializable]
    public class Character
    {
        public string name;
        public enum AnimationType { Trigger, Int, Float, Bool }
        public List<AnimationInvoker> animationInvokers = new List<AnimationInvoker>();

        public Character()
        {
            animationInvokers.Add(new AnimationInvoker());
        }

        [System.Serializable]
        public class AnimationInvoker
        {
            public string animationParameter;
            public AnimationType type;
            public int animationInt;
            public float animationFloat;
            public bool animationBool;
        }

        public void SetAnimation(Animator animator)
        {
            foreach (var item in animationInvokers)
            {
                switch (item.type)
                {
                    case Character.AnimationType.Trigger:
                        animator.SetTrigger(item.animationParameter);
                        break;
                    case Character.AnimationType.Int:
                        animator.SetInteger(item.animationParameter, item.animationInt);
                        break;
                    case Character.AnimationType.Float:
                        animator.SetFloat(item.animationParameter, item.animationFloat);
                        break;
                    case Character.AnimationType.Bool:
                        animator.SetBool(item.animationParameter, item.animationBool);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
