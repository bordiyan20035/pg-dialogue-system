﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace PG.DialougeSystem
{
    public class PGDialougeTrigger : MonoBehaviour
    {
        [SerializeField] private string _languageParameter = "Language";
        [SerializeField] private PGDialougeSystem _dialougeSystem;
        [SerializeField] private BaseDialougeProfile _profile;
        [SerializeField] private bool _activatingByTrigger;
        [SerializeField] private bool _deactivatingAfterActivate;
        [SerializeField] private InputActionProperty _InteractKey;
        [SerializeField] private string[] _tags;
        [SerializeField] private GameObject[] _interactObjects;
        [SerializeField] private UnityEvent _startEvent;
        [SerializeField] private UnityEvent _endEvent;
        private bool _isEnter;

        private void OnEnable()
        {
            _InteractKey.action.performed += InputOnInteract;
            _endEvent.AddListener(() => _isEnter = false);
        }

        private void OnDisable()
        {
            _InteractKey.action.performed -= InputOnInteract;
            _endEvent.RemoveListener(() => _isEnter = false);
            _isEnter = false;
        }

        void InputOnInteract(InputAction.CallbackContext context)
        {
            OnInteract();
        }
        public void OnInteract()
        {
            if (_isEnter)
            {
                ActivateFullDialouge();
            }
        }

        public async void ActivateFullDialouge()
        {
            _isEnter = false;
            for (int i = 0; i < _interactObjects.Length; i++)
            {
                _interactObjects[i].SetActive(false);
            }
            AssignDialogueEvents();
            await Task.Delay(60);
            StartDialogue();
            if (_deactivatingAfterActivate)
            {
                gameObject.SetActive(false);
            }
        }

        public async void ActivateFullDialouge(BaseDialougeProfile profile)
        {
            for (int i = 0; i < _interactObjects.Length; i++)
            {
                _interactObjects[i].SetActive(false);
            }
            AssignDialogueEvents();
            await Task.Delay(60);
            StartDialogue(profile);
        }

        private void AssignDialogueEvents()
        {
            _dialougeSystem.startEvent = _startEvent;
            _dialougeSystem.endEvent = _endEvent;
        }

        private void StartDialogue(BaseDialougeProfile profile = null)
        {
            profile ??= _profile;

            if (profile is PGDialougeSystemProfile systemProfile)
                _dialougeSystem?.StartDialouge(systemProfile);

            if (profile is PGDialougeProfilesFolder folderProfile)
                _dialougeSystem?.StartDialouge(folderProfile.GetElement(PlayerPrefs.GetString(_languageParameter)));
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_tags.Length > 0 && !_isEnter && enabled)
            {
                foreach (var tag in _tags)
                {
                    if (other.CompareTag(tag))
                    {
                        _isEnter = true;
                        if (_activatingByTrigger)
                        {
                            ActivateFullDialouge();
                        }
                        else
                        {
                            for (int i = 0; i < _interactObjects.Length; i++)
                            {
                                _interactObjects[i].SetActive(true);
                            }
                        }
                    }
                }
            }
        }


        private void OnTriggerExit(Collider other)
        {
            if (_tags.Length > 0 && enabled)
            {
                foreach (var tag in _tags)
                {
                    if (other.CompareTag(tag))
                    {
                        for (int i = 0; i < _interactObjects.Length; i++)
                        {
                            _interactObjects[i].SetActive(false);
                        }
                        _isEnter = false;
                    }
                }
            }
        }
    }
}
