using UnityEngine;
namespace PG.DialougeSystem
{
    [CreateAssetMenu(fileName = "New Dialouge Profiles Folder", menuName = "PG/Dialouge System/Profiles Folder")]
    public class PGDialougeProfilesFolder : BaseDialougeProfile
    {
        [field:SerializeField] public LanguageElement[] elements { get; private set; }

        public PGDialougeSystemProfile GetElement(string language)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                if (elements[i].language == language)
                {
                    return elements[i].profile;
                }
            }
            return null;
        }
    }
}
