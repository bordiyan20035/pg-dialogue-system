﻿namespace PG.DialougeSystem
{
    [System.Serializable]
    public class LanguageElementChoice
    {
        public string language;
        public string name;
        public PGDialougeSystemProfile profile;
    }
}
