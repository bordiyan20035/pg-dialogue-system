using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.InputSystem;
using Cinemachine;
using System.Threading.Tasks;
using UnityEngine.UI;
namespace PG.DialougeSystem
{
    public partial class PGDialougeSystem : MonoBehaviour
    {
        public bool isActivated {  get; private set; }
        [SerializeField] private InputActionProperty _nextDialougeKey;
        [field:SerializeField] public bool isStartOnStart { get; private set; } = true;
        [Header("Text")]
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private float _timePrint = 0.025f;
        [SerializeField] private AudioSource _audioSource;

        [SerializeField] private Button[] _choiceButtons;

        [Header("Camera")]
        [SerializeField] private bool _isCameraSetPosition;
        [SerializeField] private CinemachineVirtualCamera[] _virtualCameras;

        [Header("Animations")]
        [SerializeField] private PlayableDirector _playableDirector;
        [SerializeField] private CharacterScene[] _characters;

        [Header("Events")]
        public DialougeEvent[] dialougeEvents;
        public UnityEvent startEvent;
        [SerializeField] private PGDialougeSystemProfile _profile;
        public  UnityEvent endEvent;

        private int _currentDialouge;
        private Coroutine _coroutinePrintText;
        private bool _choicesActive => _currentDialouge == _profile.dialouges.Count - 1 && _profile.choices.Length > 0;
        private bool _checkLessIndexOfBounds => _currentDialouge < _profile.dialouges.Count;
        private bool _checkGreaterOrEqualIndexOfBounds => _currentDialouge >= _profile.dialouges.Count;

        [System.Serializable]
        public class DialougeEvent
        {
            public string name;
            public UnityEvent unityEvent;
        }
        
        // Start is called before the first frame update
        void Start()
        {
            if (isStartOnStart)
            {
                StartDialouge(_profile);
            }
        }
        private void OnEnable()
        {
            _nextDialougeKey.action.performed += InputNextDialouge;
        }
        private void OnDisable()
        {
            _nextDialougeKey.action.performed -= InputNextDialouge;
        }

        void DisplayChoices()
        {
            if (_choicesActive && _choiceButtons.Length > 0)
            {
                for (int i = 0; i < _choiceButtons.Length; i++)
                {
                    _choiceButtons[i].gameObject.SetActive(i < _profile.choices.Length);
                    _choiceButtons[i].GetComponentInChildren<TMP_Text>().text = _profile.choices[i].name;
                }
            }
        }
        public void SetDialougeChoice(int choice)
        {
            StartDialouge(_profile.choices[choice].profile);
        }
        void InputNextDialouge(InputAction.CallbackContext context)
        {
            NextDialouge();
        }
        public void SetDialouge(int value)
        {
            _currentDialouge = value;
            ShowText();
        }
        public CinemachineVirtualCamera GetElement(string Name)
        {
            for (int i = 0; i < _virtualCameras.Length; i++)
            {
                if (_virtualCameras[i].name == Name)
                {
                    return _virtualCameras[i];
                }
            }
            return null;
        }
        public void Stop()
        {
            if (_coroutinePrintText != null)
            {
                StopCoroutine(_coroutinePrintText);
            }
            isActivated = false;
            _descriptionText.text = "";
            _nameText.text = "";
        }
        [ContextMenu("End of dialouge")]
        public void EndOfDialouge()
        {
            _nameText.text = "";
            _descriptionText.text = "";
            endEvent?.Invoke();
            isActivated = false;
        }
        async void NextDialouge()
        {
            if (isActivated && isActiveAndEnabled)
            {
                if (_checkLessIndexOfBounds)
                {
                    if (_descriptionText.text.Length < _profile.GetDialouge(_currentDialouge).Length)
                    {
                        if (_coroutinePrintText != null)
                        {
                            StopCoroutine(_coroutinePrintText);
                        }

                        await Task.Delay(10);

                        _coroutinePrintText = null;
                        _descriptionText.text = _profile.GetDialouge(_currentDialouge);
                    }
                    else
                    {
                        _currentDialouge++;
                        _coroutinePrintText = null;
                        await Task.Delay(10);
                        ShowText();
                    }
                }

                // Повторная проверка после увеличения _currentDialouge
                if (_checkGreaterOrEqualIndexOfBounds)
                {
                    if (!_choicesActive || _choiceButtons.Length == 0)
                    {
                        EndOfDialouge();
                    }
                    else
                    {
                        DisplayChoices();
                    }
                }
            }
        }

        public async void StartDialouge(PGDialougeSystemProfile profile)
        {
            Stop();
            await Task.Delay(60);
            isActivated = true;
            _currentDialouge = 0;
            _profile = profile;
            await Task.Delay(60);
            ShowText();
            startEvent?.Invoke();
        }
        public async void StartDialouge()
        {
            Stop();
            await Task.Delay(60);
            _currentDialouge = 0;
            startEvent?.Invoke();
            isActivated = true;
            ShowText();
        }
        private void DialougeEventInvoke()
        {
            for (int i = 0; i < dialougeEvents.Length; i++)
            {
                if (dialougeEvents[i].name == _profile.dialouges[_currentDialouge].eventName)
                {
                    dialougeEvents[i].unityEvent?.Invoke();
                }
            }
        }
        async void ShowText()
        {
            if (isActivated && _checkLessIndexOfBounds)
            {
                PlayAudioDialouge();
                DialougeEventInvoke();

                if (_nameText != null)
                {
                    _nameText.text = _profile.GetDialougeName(_currentDialouge);
                }
                if (_descriptionText != null)
                {
                    await Task.Delay(20);
                    _coroutinePrintText = StartCoroutine(ShowTextEnumerator(_profile.GetDialouge(_currentDialouge)));
                    //Debug.Log(_coroutinePrintText, this);
                }
                PlayCutscene();
                for (int i = 0; i < _profile.dialouges[_currentDialouge].characters.Count; i++)
                {
                    AnimatorCharacterChange(i);
                }
                ChangeCameraPosition();
            }
        }

        private void ChangeCameraPosition()
        {
            if (_isCameraSetPosition && _virtualCameras.Length > 0 && _checkLessIndexOfBounds)
            {
                if (!string.IsNullOrWhiteSpace(_profile.dialouges[_currentDialouge].virtualCameraName))
                {
                    for (int i = 0; i < _virtualCameras.Length; i++)
                    {
                        _virtualCameras[i].enabled = _virtualCameras[i].name == _profile.dialouges[_currentDialouge].virtualCameraName;
                    }
                }
            }
        }

        private void PlayCutscene()
        {
            if (_playableDirector != null && _checkLessIndexOfBounds)
            {
                _playableDirector.Stop();
                if (_profile.dialouges[_currentDialouge].cutscene != null)
                {
                    _playableDirector.Play(_profile.dialouges[_currentDialouge].cutscene, DirectorWrapMode.None);
                }
            }
        }

        private void PlayAudioDialouge()
        {
            if (_audioSource != null && _checkLessIndexOfBounds)
            {
                _audioSource.Stop();
                if (_profile.dialouges[_currentDialouge].clip != null)
                {
                    _audioSource.clip = _profile.dialouges[_currentDialouge].clip;
                }
                _audioSource.Play();
            }
        }

        private void AnimatorCharacterChange(int i)
        {
            Animator animator = GetCharacter(_profile.dialouges[_currentDialouge].characters[i].name);
            if (animator != null)
            {
                _profile.dialouges[_currentDialouge].characters[i].SetAnimation(animator);
            }
        }

        public IEnumerator ShowTextEnumerator(string value)
        {
            //Debug.Log("Print Text Started", this);
            WaitForSecondsRealtime waitForSeconds = new WaitForSecondsRealtime(_timePrint);
            _descriptionText.text = "";
            for (int i = 0; i < value.Length; i++)
            {
                //Debug.Log("Print Text", this);
                _descriptionText.text += value[i];
                yield return waitForSeconds;
            }
            _descriptionText.text = value;
            //Debug.Log("Print Text Ended", this);
        }
        public Animator GetCharacter()
        {
            if (_characters.Length > 0)
            {
                foreach (var character in _characters)
                {
                    foreach (var character2 in _profile.dialouges[_currentDialouge].characters)
                    {
                        if (character.name == character2.name)
                        {
                            return character.animator;
                        }
                    }
                }
            }
            return null;
        }
        public Animator GetCharacter(string name)
        {
            if (_characters.Length > 0)
            {
                foreach (var character in _characters)
                {
                    if (character.name == name)
                    {
                        return character.animator;
                    }
                }
            }
            return null;
        }
    }
}
