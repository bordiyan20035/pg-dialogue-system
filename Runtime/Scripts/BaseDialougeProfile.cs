﻿using UnityEngine;
namespace PG.DialougeSystem
{
    public abstract class BaseDialougeProfile : ScriptableObject
    {


        public virtual string GetDialouge(int id)
        {
            return "";
        }
        public virtual string GetDialougeName(int id)
        {
            return "";
        }
    }
}
