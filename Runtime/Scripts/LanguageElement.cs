﻿namespace PG.DialougeSystem
{
    [System.Serializable]
    public class LanguageElement
    {
        public string language;
        public PGDialougeSystemProfile profile;
    }
}
