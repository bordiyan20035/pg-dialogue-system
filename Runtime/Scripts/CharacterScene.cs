﻿using UnityEngine;
namespace PG.DialougeSystem
{
    public partial class PGDialougeSystem
    {
        [System.Serializable]
        public class CharacterScene
        {
            public string name;
            public Animator animator;
        }
    }
}
