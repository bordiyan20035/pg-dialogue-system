using System.Collections.Generic;
using UnityEngine;

namespace PG.DialougeSystem
{
    [CreateAssetMenu(fileName = "New Dialouge System Profile", menuName = "PG/Dialouge System/Profile")]
    public class PGDialougeSystemProfile : BaseDialougeProfile
    {
        public LanguageElementChoice[] choices;
        [HideInInspector] public List<Dialouge> dialouges = new List<Dialouge>();
        public override string GetDialouge(int id)
        {
            return dialouges[id].description;
        }
        public override string GetDialougeName(int id)
        {
            return dialouges[id].name;
        }
    }
}
