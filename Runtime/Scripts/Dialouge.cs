﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

namespace PG.DialougeSystem
{
    [System.Serializable]
    public class Dialouge
    {
        public string name;
        [TextArea(3, 10)] public string description;
        public string virtualCameraName;


        public string eventName;
        public AudioClip clip;
        public TimelineAsset cutscene;


        public List<Character> characters = new List<Character>();
        public Dialouge()
        {

        }
        // Конструктор копирования
        public Dialouge(Dialouge original)
        {
            name = original.name;
            description = original.description;
            clip = original.clip;
            cutscene = original.cutscene;

            characters = new List<Character>();
            foreach (var character in original.characters)
            {
                characters.Add(new Character
                {
                    name = character.name
                });
            }
        }
    }
}
