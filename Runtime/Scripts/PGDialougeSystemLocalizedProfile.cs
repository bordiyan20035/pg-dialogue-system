﻿using UnityEngine;
using PG.LocalizationManagement;
namespace PG.DialougeSystem
{
    [CreateAssetMenu(fileName = "New Dialouge System Localized Profile", menuName = "PG/Dialouge System/Localized Profile")]
    public class PGDialougeSystemLocalizedProfile : PGDialougeSystemProfile
    {
        public override string GetDialouge(int id)
        {
            return LocalizationManager.Instance.GetLocalizedValue(dialouges[id].description);
        }
        public override string GetDialougeName(int id)
        {
            return LocalizationManager.Instance.GetLocalizedValue(dialouges[id].name);
        }
    }
}
